package com.pccw.backend.bean;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * SearchCondition
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class BaseBean implements Serializable {
    private long createAt;
    private long createBy;
    private long updateAt;
    private long updateBy;
    private String active;
    private String createAccountName;
    private String updateAccountName;
}
