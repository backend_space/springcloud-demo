package com.pccw.backend;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;

/**
 * @author: qiang
 * @date: 2020-04-26
 * @version: 1.0
 */
@SpringBootApplication(exclude = DataSourceAutoConfiguration.class)
public class SMPCommonApplication {
    public static void main(String[] args) {
        SpringApplication.run(SMPCommonApplication.class,args);
    }
}
