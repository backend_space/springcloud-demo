package com.pccw.backend;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;

/**
 * @author: qiang
 * @date: 2020-04-21
 * @version: 1.0
 */
@SpringBootApplication()
public class SMPMasterfileRepoServiceApplication {
    public static void main(String[] args) {
        SpringApplication.run(SMPMasterfileRepoServiceApplication.class,args);
    }
}
