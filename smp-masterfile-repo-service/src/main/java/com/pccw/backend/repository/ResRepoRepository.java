package com.pccw.backend.repository;


import com.pccw.backend.entity.DbResRepo;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;

@Repository
public interface ResRepoRepository extends BaseRepository<DbResRepo> {


    DbResRepo findFirst1ByRepoCode(String repo_id);
}