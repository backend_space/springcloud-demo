package com.backend.springcloud.service;

import com.backend.springcloud.domain.Product;

import java.util.List;

/**
 * @author: qiang
 * @date: 2020-03-31
 * @version: 1.0
 */
public interface ProductService {

    List<Product> listProduct();

    Product findById(int id);
}
