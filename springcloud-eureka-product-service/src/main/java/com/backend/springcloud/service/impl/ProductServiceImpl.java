package com.backend.springcloud.service.impl;

import com.backend.springcloud.domain.Product;
import com.backend.springcloud.service.ProductService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.util.*;

/**
 * @author: qiang
 * @date: 2020-03-31
 * @version: 1.0
 */
@Service
public class ProductServiceImpl implements ProductService {


    private static final Map<Integer, Product> daoMap = new HashMap<>();
    private final Logger logger = LoggerFactory.getLogger(getClass());

    static{
        Product p1 = new Product(1,"iphone 11",8000,10);
        Product p2 = new Product(2,"华为P40",5999,210);
        Product p3 = new Product(3,"华为Mate30",3599,140);
        Product p4 = new Product(4,"荣耀V30",2599,105);
        Product p5 = new Product(5,"荣耀20 Pro",2300,610);
        Product p6 = new Product(6,"小米10",4550,103);
        Product p7 = new Product(7,"OPPO",3750,130);
        Product p8 = new Product(8,"VIVO",3420,101);
        daoMap.put(p1.getId(),p1);
        daoMap.put(p2.getId(),p2);
        daoMap.put(p3.getId(),p3);
        daoMap.put(p4.getId(),p4);
        daoMap.put(p5.getId(),p5);
        daoMap.put(p6.getId(),p6);
        daoMap.put(p7.getId(),p7);
        daoMap.put(p8.getId(),p8);

    }

    @Override
    public List<Product> listProduct() {
        Collection<Product> collection = daoMap.values();
        ArrayList<Product> list = new ArrayList<>(collection);
        return list;
    }

    @Override
    public Product findById(int id) {
        logger.info("---service findbyid---");
        return daoMap.get(id);
    }
}
