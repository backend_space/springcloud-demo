package com.backend.springcloud.controller;

import com.backend.springcloud.domain.Product;
import com.backend.springcloud.service.ProductService;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author: qiang
 * @date: 2020-03-31
 * @version: 1.0
 */
@RestController
@RequestMapping("/api/product")
@RefreshScope
public class ProductController {


    @Value("${server.port}")
    private  String port;

    @Value("${env}")
    private  String env;

    @Autowired
    private ProductService productService;

    @RequestMapping("list")
    public Object list(){
        return productService.listProduct();
    }

    @RequestMapping("hi")
    public String say(){
        System.out.println("say hello from product service!!!!!!");
        return "hello";
    }

    @RequestMapping("find")
    public Object findById(@RequestParam("id") int id){

/*        try {
            TimeUnit.SECONDS.sleep(2);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }*/

        Product product = productService.findById(id);
        Product p = new Product();
        BeanUtils.copyProperties(product,p);
        p.setName(p.getName() + " data from port = " + port + ","+ " env = " + env);
        return  p;
    }
}
