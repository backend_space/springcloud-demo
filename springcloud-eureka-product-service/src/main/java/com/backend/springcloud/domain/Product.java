package com.backend.springcloud.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author: qiang
 * @date: 2020-03-31
 * @version: 1.0
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Product {

    private int id;

    private String name;

    private int price;

    private int store;
}
