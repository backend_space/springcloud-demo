package com.backend.springcloud.controller;

import com.backend.springcloud.service.ProductOrderService;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

/**
 * @author: qiang
 * @date: 2020-03-31
 * @version: 1.0
 */

@RestController
@RequestMapping("api/order")
public class OrderController {

    @Autowired
    private ProductOrderService productOrderService;

    @Autowired
    private StringRedisTemplate stringRedisTemplate;

    @RequestMapping("save" )
    @HystrixCommand(fallbackMethod = "saveOrderFail")
    public Object save(@RequestParam("user_id") int userId, @RequestParam("product_id") int productId, HttpServletRequest request) {

        String token = request.getHeader("token");
        String cookie = request.getHeader("cookie");
        System.out.println("token:" + token);
        System.out.println("cookie:" + cookie);

        Map<String, Object> data = new HashMap<>();
        data.put("code", 0);
        data.put("data", productOrderService.save(userId, productId));
        return data;
    }

    private  Object saveOrderFail(int userId, int productId, HttpServletRequest request){

        //监控报警
        String saveOrderKey = "save-order";
        String sendValue = stringRedisTemplate.opsForValue().get(saveOrderKey);
        String ip = request.getRemoteAddr();
        new Thread(()->{
            if (StringUtils.isBlank(sendValue)){
                System.out.println("紧急短信：用户下单失败，请查找原因,地址是:" + ip);
                stringRedisTemplate.opsForValue().set(saveOrderKey,"save-order-fail",20, TimeUnit.SECONDS);
            }else {
                System.out.println("已发送短信，20秒内不重复发送");
            }
        }).start();


        Map<String,Object> msg = new HashMap<>();
        msg.put("code",-1);
        msg.put("msg","人数太多，请稍后重试！！！！");
        return msg;
    }
}
