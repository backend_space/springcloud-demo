package com.backend.springcloud.utils;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.IOException;

/**
 * @author: qiang
 * @date: 2020-04-02
 * @version: 1.0
 */
public class JsonUtils {

    private static final ObjectMapper objectMapper = new ObjectMapper();

    public static JsonNode str2JsonNode(String str){
        try {
            return objectMapper.readTree(str);
        }catch (IOException e){
            return null;
        }
    }
}
