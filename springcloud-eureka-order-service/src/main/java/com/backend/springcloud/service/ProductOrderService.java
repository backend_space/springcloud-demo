package com.backend.springcloud.service;

import com.backend.springcloud.domain.ProductOrder;

/**
 * @author: qiang
 * @date: 2020-03-31
 * @version: 1.0
 */

public interface ProductOrderService {
    /**
     * 下单接口
     *
     * @param userId
     * @param productId
     * @return
     */
    ProductOrder save(int userId, int productId);
}
