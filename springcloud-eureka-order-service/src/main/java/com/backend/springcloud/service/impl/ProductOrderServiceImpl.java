package com.backend.springcloud.service.impl;

import com.backend.springcloud.domain.ProductOrder;
import com.backend.springcloud.service.ProductClient;
import com.backend.springcloud.service.ProductOrderService;
import com.fasterxml.jackson.databind.JsonNode;
import com.pccw.backend.util.JsonUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.UUID;

/**
 * @author: qiang
 * @date: 2020-04-12
 * @version: 1.0
 */


@Service
public class ProductOrderServiceImpl implements ProductOrderService {


    private final Logger logger = LoggerFactory.getLogger(getClass());

    @Autowired
    private ProductClient productClient;

    /**
     * 下单接口
     *
     * @param userId
     * @param productId
     * @return
     */
    @Override
    public ProductOrder save(int userId, int productId) {

        if(userId == 1){
            return null;
        }

        // 调用订单服务
        String response = productClient.findById(productId);

        logger.info("service save order");
        JsonNode jsonNode = JsonUtil.str2JsonNode(response);

       /* Map<String,Object> productMap = restTemplate.getForObject("http://product-service/api/v1/product/find?id= "+ productId, Map.class);

        System.out.println("productMap:" + productMap);*/

        ProductOrder productOrder = new ProductOrder();
        productOrder.setCreateTime(new Date());
        productOrder.setUserId(userId);
        productOrder.setTradeNo(UUID.randomUUID().toString());
        productOrder.setProductName(jsonNode.get("name").toString());
        productOrder.setPrice(Integer.parseInt(jsonNode.get("price").toString()));
        return productOrder;
    }

}