package com.backend.springcloud.service;

import com.backend.springcloud.fallback.ProductClientFallback;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * @author: qiang
 * @date: 2020-04-02
 * @version: 1.0
 */

/**
 商品服务客户端
 */
@FeignClient(name = "product-service",fallback = ProductClientFallback.class)
public interface ProductClient {
    @GetMapping("api/product/find")
    String findById(@RequestParam(value = "id") int id);
}
