package com.backend.springcloud.domain;

import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * @author: qiang
 * @date: 2020-03-31
 * @version: 1.0
 */
@Data
public class ProductOrder implements Serializable {

    private int id;

    private String productName;

    private String tradeNo;

    private int price;

    private Date createTime;

    private int userId;

}
