package com.backend.springcloud;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.cloud.client.circuitbreaker.EnableCircuitBreaker;
import org.springframework.cloud.netflix.hystrix.dashboard.EnableHystrixDashboard;
import org.springframework.cloud.openfeign.EnableFeignClients;

@SpringBootApplication(exclude = DataSourceAutoConfiguration.class)
@EnableFeignClients
@EnableCircuitBreaker
@EnableHystrixDashboard
public class SpringcloudEurekaOrderServiceApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringcloudEurekaOrderServiceApplication.class, args);
    }
}
