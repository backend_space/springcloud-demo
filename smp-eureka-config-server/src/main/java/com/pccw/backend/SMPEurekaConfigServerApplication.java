package com.pccw.backend;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.cloud.config.server.EnableConfigServer;

/**
 * @author: qiang
 * @date: 2020-04-20
 * @version: 1.0
 */
@SpringBootApplication(exclude = DataSourceAutoConfiguration.class)
@EnableConfigServer
public class SMPEurekaConfigServerApplication {
    public static void main(String[] args) {
        SpringApplication.run(SMPEurekaConfigServerApplication.class,args);
    }
}