一:微服务启动顺序：
   
1.     smp-eureka-erver
1.     smp-eureka-config-server
1.     springcloud-eureka-order-service
1.     springcloud-eureka-produce-service
1.     smp-masterfile-repo-service
1.     smp-eureka-zuul-gateway

    
二：此demo结合了Ribbon、Feign、Hystrix、Sleuth、Zipkin、分布式Config配置、Bus消息总线、RabbitMQ等组件,
    在运行demo前得确保启动Redis、Zipkin、RabbitMQ等服务
    
三：把相应的配置文件导入自己的gitlab。

四：如果需要多节点启动，只需要在yml配置文件中更改个端口再重新启动程序即可

五：Hystrix Dashboard：http://localhost:8782/hystrix
    输入：http://localhost:8782/actuator/hystrix.stream 
    
六：可视化链路追踪：
    启动Zipkin: http://localhost:9411/zipkin/

七：消息总线Bus结合config组件搭建配置中⼼操作流程

    RabbitMQ账号密码默认guest/guest

    启动rabbitmq:  docker run -d -p 5672:5672 -p 15672:15672 rabbitmq:management
    
    访问验证（通过Postman或者gitlab的webhook）：http://localhost:8772/actuator/bus-refresh